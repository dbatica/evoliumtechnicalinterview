package com.evolio.app.evoliotestapp;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

public class MainActivity extends AppCompatActivity {


    ImageView mImageHolder1;
    ImageView mImageHolder2;

    Handler mHandlerLoad2;
    Runnable mRunnableLoad2;

    ProgressBar mProgressBar1;
    ProgressBar mProgressBar2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mImageHolder1 = (ImageView) findViewById(R.id.firstImageView);
        mImageHolder2 = (ImageView) findViewById(R.id.secondImageView);

        mProgressBar1 = (ProgressBar) findViewById(R.id.firstActivityIndicator);
        mProgressBar2 = (ProgressBar) findViewById(R.id.secondActivityIndicator);

        mHandlerLoad2 = new Handler();

        mRunnableLoad2 = new Runnable() {
            @Override
            public void run() {
                loadingImage(mImageHolder2, R.drawable.big_image, mProgressBar2);
            }
        };

    }

    private void loadingImage(ImageView img, int resId, ProgressBar progressBar) {

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;

        if (height > width) {// if portrait
            height /= 2;
        } else {// if landscape
            width /= 2;
        }

        Bitmap bitmap = decodeSampledBitmapFromResource(getResources(), resId, width, height);
        progressBar.setVisibility(View.GONE);
        img.setImageBitmap(bitmap);
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    @Override
    protected void onStart() {
        super.onStart();

        loadingImage(mImageHolder1, R.drawable.girl_png, mProgressBar1);
        mHandlerLoad2.postDelayed(mRunnableLoad2, 3 * 1000);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mHandlerLoad2.removeCallbacks(mRunnableLoad2);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
